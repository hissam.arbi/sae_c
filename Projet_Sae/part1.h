#pragma once
struct date_{
 int jours;
 int mois;
 int annee;
};
typedef struct date_ date;
struct liste_p_{
    char nom[15];
    char prenom[15];
    double prix;
    int siege;
    date date_naissance;
};
typedef struct liste_p_ liste_p;
struct vol_ {
    int numero;
    char comp[20];
    char dest[50];
    int num_compt;
    int heure_deb_enr;
    int heure_fin_enr;
    int num_salle;
    int heure_deb_emb;
    int heure_fin_emb;
    int heure_deco;
    char etat[20];
    liste_p passagers[50];

};
typedef struct vol_ vol;


