#include"part1.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lecture.h"
void tri_insertion( vol*vols,int nb_vol) {
    int i, j, cle;
    for (i = 2; i < nb_vol; i++) {
        cle = vols[i].heure_deco;
        j = i - 1;

        while (j >= 0 && vols[j].heure_deco > cle) {
            echange(j+1,j,vols);
            j = j - 1;
        }

        vols[j + 1].heure_deco = cle;
    }
}
void echange(int a,int b,vol*vols){
    vol temp;
    temp.numero=vols[a].numero;
        strcpy(temp.comp,vols[a].comp);
        strcpy(temp.dest,vols[a].dest);
        temp.num_compt=vols[a].num_compt;
        temp.heure_deb_enr=vols[a].heure_deb_enr;
        temp.heure_fin_enr=vols[a].heure_fin_enr;
        temp.num_salle=vols[a].num_salle;
        temp.heure_deb_emb=vols[a].heure_deb_emb;
        temp.heure_deco=vols[a].heure_deco;
        temp.heure_fin_emb=vols[a].heure_fin_emb;
        strcpy(temp.etat,vols[a].etat);

            vols[a].numero = vols[b].numero;
    strcpy(vols[a].comp, vols[b].comp);
    strcpy(vols[a].dest, vols[b].dest);
    vols[a].num_compt = vols[b].num_compt;
    vols[a].heure_deb_enr = vols[b].heure_deb_enr;
    vols[a].heure_fin_enr = vols[b].heure_fin_enr;
    vols[a].num_salle = vols[b].num_salle;
    vols[a].heure_deb_emb = vols[b].heure_deb_emb;
    vols[a].heure_fin_emb = vols[b].heure_fin_emb;
    vols[a].heure_deco = vols[b].heure_deco;
    strcpy(vols[a].etat, vols[b].etat);


    vols[b].numero = temp.numero;
    strcpy(vols[b].comp, temp.comp);
    strcpy(vols[b].dest, temp.dest);
    vols[b].num_compt = temp.num_compt;
    vols[b].heure_deb_enr = temp.heure_deb_enr;
    vols[b].heure_fin_enr = temp.heure_fin_enr;
    vols[b].num_salle = temp.num_salle;
    vols[b].heure_deb_emb = temp.heure_deb_emb;
    vols[b].heure_fin_emb = temp.heure_fin_emb;
    vols[b].heure_deco = temp.heure_deco;
    strcpy(vols[b].etat, temp.etat);


    return;
}

int affiche_vol(int heure_act, vol* vols, int nb_vol){

    int i =1;
    while(i <= nb_vol){
    if(vols[i].heure_deco - heure_act <= 300 && vols[i].heure_deco - heure_act >= -10 ){
            printf("%d, %d, %s, %s, ", vols[i].heure_deco, vols[i].numero, vols[i].comp, vols[i].dest);
            if(vols[i].heure_deb_enr - heure_act <=30 && vols[i].heure_deb_enr - heure_act >=-10){
            printf("%d, %d , %d,",vols[i].num_compt,vols[i].heure_deb_enr,vols[i].heure_fin_enr);}
        if(vols[i].heure_deb_emb - heure_act<=30 && vols[i].heure_deb_emb - heure_act>= -10){
            printf("%d ,%d ,%d ,",vols[i].num_salle,vols[i].heure_deb_emb,vols[i].heure_fin_emb);
        }
        printf("%s\n",vols[i].etat);
    }

    i++;}




}



int recherche_vol_comp(char compagnie[], int taille, vol* vols,vol* vol_concerne){
    int i = 0,j=0;
    while( i<= taille){
        if( strcmp(vols[i].comp,compagnie)==0){
            vol_concerne[j].numero = vols[i].numero;
            strcpy(vol_concerne[j].comp,vols[i].comp);
            strcpy(vol_concerne[j].dest,vols[i].dest);
            vol_concerne[j].num_compt = vols[i].num_compt;
            vol_concerne[j].heure_deb_enr = vols[i].heure_deb_enr;
            vol_concerne[j].heure_fin_enr = vols[i].heure_fin_enr;
            vol_concerne[j].num_salle = vols[i].num_salle;
            vol_concerne[j].heure_deb_emb = vols[i].heure_deb_emb;
            vol_concerne[j].heure_fin_emb = vols[i].heure_fin_emb;
            vol_concerne[j].heure_deco = vols[i].heure_deco;
            strcpy(vol_concerne[j].etat,vols[i].etat);
            j++;

        }
        i++;


    }
    return j ;
    /*int t=0;
    while(t<j){
        printf("%d\t",vol_concerne[t].numero);
        printf("%s\t",vol_concerne[t].comp);
        printf("%s\t",vol_concerne[t].dest);
        printf("%d\t",vol_concerne[t].num_compt);
        printf("%d\t",vol_concerne[t].heure_deb_enr);
        printf("%d\t",vol_concerne[t].heure_fin_enr);
        printf("%d\t",vol_concerne[t].num_salle);
        printf("%d\t",vol_concerne[t].heure_deb_emb);
        printf("%d\t",vol_concerne[t].heure_fin_emb);
        printf("%d\t",vol_concerne[t].heure_deco);
        printf("%s\t\n",vol_concerne[t].etat);
        t++;
     }*/


}
int recherche_vol_destination(char destination[], int taille, vol* vols,vol* vol_concerne){

    int i = 0,j=0;
    while( i< taille){
        if( strcmp(vols[i].dest,destination)==0){
            vol_concerne[j].numero = vols[i].numero;
            strcpy(vol_concerne[j].comp,vols[i].comp);
            strcpy(vol_concerne[j].dest,vols[i].dest);
            vol_concerne[j].num_compt = vols[i].num_compt;
            vol_concerne[j].heure_deb_enr = vols[i].heure_deb_enr;
            vol_concerne[j].heure_fin_enr = vols[i].heure_fin_enr;
            vol_concerne[j].num_salle = vols[i].num_salle;
            vol_concerne[j].heure_deb_emb = vols[i].heure_deb_emb;
            vol_concerne[j].heure_fin_emb = vols[i].heure_fin_emb;
            vol_concerne[j].heure_deco = vols[i].heure_deco;
            strcpy(vol_concerne[j].etat,vols[i].etat);
            j++;
        }
        i++;

    }
    return j ;

}
int  recherche_heure(int  heure, int taille, vol* vols,vol* vol_concerne){
    int i = 0,j=0;
    while( i< taille){
        if( vols[i].heure_deco == heure){
            vol_concerne[j].numero = vols[i].numero;
            strcpy(vol_concerne[j].comp,vols[i].comp);
            strcpy(vol_concerne[j].dest,vols[i].dest);
            vol_concerne[j].num_compt = vols[i].num_compt;
            vol_concerne[j].heure_deb_enr = vols[i].heure_deb_enr;
            vol_concerne[j].heure_fin_enr = vols[i].heure_fin_enr;
             vol_concerne[j].num_salle = vols[i].num_salle;
            vol_concerne[j].heure_deb_emb = vols[i].heure_deb_emb;
            vol_concerne[j].heure_fin_emb = vols[i].heure_fin_emb;
            vol_concerne[j].heure_deco = vols[i].heure_deco;
            strcpy(vol_concerne[j].etat,vols[i].etat);
            j++;
        }
        i++;

    }
    return j++;

}


void affiche_embarque(vol* vols, int nb_passager){
    int i,k=0,e=0,a=0, nb_enfant = 10, nb_adulte=20;
    liste_p enfant[nb_enfant];
    liste_p adulte[nb_adulte];
    printf("Entrez le numero du vol concerne");
    scanf("%d",&i);
    while(k < nb_passager){
        if (convert_age(vols, i, k) < 12){
                strcpy(enfant[e].nom, vols[i].passagers[k].nom);
                strcpy(enfant[e].prenom, vols[i].passagers[k].prenom);
                enfant[e].prix= vols[i].passagers[k].prix;
                enfant[e].siege= vols[i].passagers[k].siege;
                enfant[e].date_naissance.jours= vols[i].passagers[k].date_naissance.jours;
                enfant[e].date_naissance.mois= vols[i].passagers[k].date_naissance.mois;
                enfant[e].date_naissance.annee= vols[i].passagers[k].date_naissance.annee;
                e++;
            }
        else {
                strcpy(adulte[a].nom, vols[i].passagers[k].nom);
                strcpy(adulte[a].prenom, vols[i].passagers[k].prenom);
                adulte[a].prix= vols[i].passagers[k].prix;
                adulte[a].siege= vols[i].passagers[k].siege;
                adulte[a].date_naissance.jours= vols[i].passagers[k].date_naissance.jours;
                adulte[a].date_naissance.mois= vols[i].passagers[k].date_naissance.mois;
                adulte[a].date_naissance.annee= vols[i].passagers[k].date_naissance.annee;
                a++;
            }
        k++;

    }

    int cpt = 0;
    int d ;
    while( cpt < e && e>=2){
         d=cpt+1;

        while( d < e){
            if(comparateur_prix(enfant[cpt],enfant[d])==1){
                     echange_passanger(enfant,cpt,d);

            d++;
            }
            else{
                d++;}

        }
        cpt++;
    }
    cpt = 0;
    while( cpt < e && e>=2){
         d=cpt+1;

        while( d < e){
          if(comparateur_prix(enfant[cpt],enfant[d])==0){
                if(comparateur_nom(enfant[cpt],enfant[d])==0){
                     echange_passanger(enfant,cpt,d);

                    d++;}

                else{
                    d++;
                }

            }
             else{
                    d++;
                }
        }

        cpt++;
    }
     cpt = 0;
    while( cpt < a && a>=2){
         d=cpt+1;

        while( d < a){
            if(comparateur_prix(adulte[cpt],adulte[d])==1){
                     echange_passanger(adulte,cpt,d);

            d++;
            }
                d++;

        }
        cpt++;
    }
    cpt = 0;
    while( cpt < a && a>=2){
         d=cpt+1;

        while( d < a){
            if(comparateur_prix(adulte[cpt],adulte[d])==0){
                if(comparateur_nom(adulte[cpt],adulte[d])==0){
                     echange_passanger(adulte,cpt,d);

            d++;
                }
                else{
                    d++;
                }
            }
            else{d++;}


        }
        cpt++;
    }
cpt =0;
while(cpt < e){
    printf("%s,%d,%lf\n",enfant[cpt].nom,enfant[cpt].date_naissance.annee,enfant[cpt].prix);
    cpt++;
}
cpt=0;
while(cpt < a){
    printf("%s,%d,%lf\n",adulte[cpt].nom,adulte[cpt].date_naissance.annee,adulte[cpt].prix);
    cpt++;
}


    }




int convert_age (vol* vols, int i, int k ){
    return (2024 - vols[i].passagers[k].date_naissance.annee);


}
int comparateur_prix(liste_p pass_1,liste_p pass_2){
    if(pass_1.prix == pass_2.prix){
            return 0;
            }
    else{
        if(pass_1.prix < pass_2.prix){
            return 1 ;

        }
        else{return -1;}

    }



}
void echange_passanger( liste_p *p_e1, int position, int position2){
        //Variable temporaire qui stocke e1
        liste_p temp;
        strcpy(temp.nom, p_e1[position].nom);
        strcpy(temp.prenom, p_e1[position].prenom);
        temp.prix= p_e1[position].prix;
        temp.siege= p_e1[position].siege;
        temp.date_naissance.jours= p_e1[position].date_naissance.jours;
        temp.date_naissance.mois= p_e1[position].date_naissance.mois;
        temp.date_naissance.annee= p_e1[position].date_naissance.annee;
        //debut echange entre e1 et e2 (e1 prend les valeurs de e2)
        strcpy( p_e1[position].nom, p_e1[position2].nom);
        strcpy( p_e1[position].prenom, p_e1[position2].prenom);
         p_e1[position].prix= p_e1[position2].prix;
         p_e1[position].siege= p_e1[position2].siege;
         p_e1[position].date_naissance.jours= p_e1[position2].date_naissance.jours;
         p_e1[position].date_naissance.mois= p_e1[position2].date_naissance.mois;
         p_e1[position].date_naissance.annee= p_e1[position2].date_naissance.annee;
        //fin echange entre e1 et e2 (e2 prend les valeurs de e1 stocke dans la variable temp)
        strcpy(p_e1[position2].nom, temp.nom);
        strcpy(p_e1[position2].prenom, temp.prenom);
        p_e1[position2].prix= temp.prix;
        p_e1[position2].siege= temp.siege;
        p_e1[position2].date_naissance.jours= temp.date_naissance.jours;
        p_e1[position2].date_naissance.mois= temp.date_naissance.mois;
        p_e1[position2].date_naissance.annee= temp.date_naissance.annee;


}
int comparateur_nom(liste_p pass_e1,liste_p pass_e2){
      int i;
        do{
            if(pass_e1.nom[i]< pass_e2.nom[i]){
                return 0;
            }
            else{
                if(pass_e1.nom[i] > pass_e2.nom[i]){
                    return 1;
            }
                else{
                    i++;
                }

            }
        }while(pass_e1.nom[i]== pass_e2.nom[i]);


}

void retard (vol*vols, int nb_vols){
    vol vol_heure[40],en_retard[40],vol_annule[40], vol_reprograme[40];
    char trash[10];
    int min_retard[40];
    int i=1,j=0, r=0, a=0;
    while(i< nb_vols){
        if(strcmp(vols[i].etat,"A l'heure")==0){
            vol_heure[j].numero = vols[i].numero;
            strcpy(vol_heure[j].comp,vols[i].comp);
            strcpy(vol_heure[j].dest,vols[i].dest);
            vol_heure[j].num_compt = vols[i].num_compt;
            vol_heure[j].heure_deb_enr = vols[i].heure_deb_enr;
            vol_heure[j].heure_fin_enr = vols[i].heure_fin_enr;
            vol_heure[j].heure_deb_emb = vols[i].heure_deb_emb;
            vol_heure[j].heure_fin_emb = vols[i].heure_fin_emb;
            vol_heure[j].num_salle = vols[i].num_salle;
            vol_heure[j].heure_deco = vols[i].heure_deco;
            strcpy(vol_heure[j].etat,vols[i].etat);
            j++;
        }
        else{
            if(strcmp(vols[i].etat,"Annule")==0){
                vol_annule[a].numero = vols[i].numero;
                strcpy(vol_annule[a].comp,vols[i].comp);
                strcpy(vol_annule[a].dest,vols[i].dest);
                vol_annule[a].num_compt = vols[i].num_compt;
                vol_annule[a].heure_deb_enr = vols[i].heure_deb_enr;
                vol_annule[a].heure_fin_enr = vols[i].heure_fin_enr;
                vol_annule[a].heure_deb_emb = vols[i].heure_deb_emb;
                vol_annule[a].heure_fin_emb = vols[i].heure_fin_emb;
                vol_annule[a].num_salle = vols[i].num_salle;
                vol_annule[a].heure_deco = vols[i].heure_deco;
                strcpy(vol_annule[a].etat,vols[i].etat);
                a++;

        }
            else{
                en_retard[r].numero = vols[i].numero;
                strcpy(en_retard[r].comp,vols[i].comp);
                strcpy(en_retard[r].dest,vols[i].dest);
                en_retard[r].num_compt = vols[i].num_compt;
                en_retard[r].heure_deb_enr = vols[i].heure_deb_enr;
                en_retard[r].heure_fin_enr = vols[i].heure_fin_enr;
                en_retard[r].heure_deb_emb = vols[i].heure_deb_emb;
                en_retard[r].heure_fin_emb = vols[i].heure_fin_emb;
                en_retard[r].heure_deco = vols[i].heure_deco;
                en_retard[r].num_salle = vols[i].num_salle;
                strcpy(en_retard[r].etat,vols[i].etat);
                r++;

            }
        }
        i++;
    }
    tri_insertion(vol_heure,j);
    tri_insertion(en_retard,r);
    int t = 0;
    int k = 0;
    int d=1;
    while(t < r){
            strcpy(trash,strtok(en_retard[t].etat,"("));
            min_retard[t] = atoi(strtok(NULL, " "));
            k=0;
     while(strcmp(en_retard[t].etat,"Reprogramme")!=0 && strcmp(en_retard[t].etat,"Annule")!=0){
        if(en_retard[t].heure_deco + min_retard[t] - vol_heure[k].heure_deco >=5 ){
            if(en_retard[t].heure_deco + min_retard[t] - vol_heure[k+1].heure_deco <= -5 ){
                    printf("%d\n",min_retard[t]);
                  en_retard[t].heure_deco =  en_retard[t].heure_deco + min_retard[t];
                  printf("%d",en_retard[0].heure_deco);
                  strcpy(en_retard[t].etat,"Reprogramme");
                  copy_struct(vol_heure,en_retard,j+d,t);
                  tri_insertion(vol_heure, j+d);
                  d++;


            }
            else{
                k++;
            }
        }
        else{
                if(en_retard[t].heure_deco + min_retard[t]>2200){
                    strcpy(en_retard[t].etat,"Annule");
                }
             else{
                min_retard[t]=min_retard[t] +1;
             }
        }

        }
        t++;
    }
//printf(",Son numero :%d, son etat %s son heure de deco: %d",en_retard[0].numero, en_retard[0].etat,en_retard[0].heure_deco);


}
void copy_struct(vol* vols, vol* en_retard,int pos_vol, int pos_retard){
                vols[pos_vol].numero = en_retard[pos_retard].numero;
                strcpy(vols[pos_vol].comp,en_retard[pos_retard].comp);
                strcpy(vols[pos_vol].dest,en_retard[pos_retard].dest);
                vols[pos_vol].num_compt = en_retard[pos_retard].num_compt;
                vols[pos_vol].heure_deb_enr = en_retard[pos_retard].heure_deb_enr;
                vols[pos_vol].heure_fin_enr = en_retard[pos_retard].heure_fin_enr;
                vols[pos_vol].heure_deb_emb = en_retard[pos_retard].heure_deb_emb;
                vols[pos_vol].heure_fin_emb = en_retard[pos_retard].heure_fin_emb;
                vols[pos_vol].num_salle = en_retard[pos_retard].num_salle;
                strcpy(vols[pos_vol].etat,en_retard[pos_retard].etat);

}

































/*char substring(char myline[]){
int i,j;
char passengersToken[strlen(myline)];
for(i=0; i <strlen(myline);i++){
    if(myline[i]=='\"'){
        i++;
        while(myline[i]!=';'){
            passengersToken[j]= myline[i];
            j++;
            i++;

            }
        }
    }
    passengersToken[j]='\0';
    printf("%s",passengersToken);
    return passengersToken;
}*/
/*char substring2(char passengersToken[], int k){
int i=0,j=0,r=0,cpt=0;
memset(passengersInfo,0,sizeof(passengersInfo));
for(i=0;i<strlen(passengersToken);i++){
    if(passengersToken[i]==';'){
        cpt;
    }
    if(k==cpt2){
            i++
        while(myline[i]!=';'){
            passengersInfo[j]= passengersToken[i];
            j++;
            i++;

            }
        break;
    }
    else{
        i++;
    }
}

*/
/*char substring2(char passengersToken[], int k,char passengersInfo[]){
int i=0,j=0,r=0,cpt=0;
memset(passengersInfo,0,sizeof(passengersInfo));
for(i=0;i<strlen(passengersToken);i++){
    if(passengersToken[i]==';'){
        cpt++;
    }
    if(k==cpt){
            i++;
        while(passengersToken[i]!=';'){
            passengersInfo[j]= passengersToken[i];
            j++;
            i++;

            }
        break;
    }
    else{
        i++;
    }
}
}

*/

