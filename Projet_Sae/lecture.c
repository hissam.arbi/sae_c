#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "part1.h"
#include "lecture.h"

int lecture(vol *vols, int nb_vols) {
    FILE *fichier = fopen("data_vols.csv", "r");
    if (fichier == NULL) {
        perror("ce fichier ne peut pas etre lu");
        return 1;
    }

    char myline[500];
    int j = 0;

    while (fgets(myline, 500, fichier) != NULL) {
        j++;
    }

    rewind(fichier);

    /*vol* vols = (vol*)malloc(j * sizeof(vol));
    if (vols == NULL) {
        perror("allocation memoire echouee");
        fclose(fichier);
        return 1;
    }*/

    int i = 0;
    char passengersInfo[50];
    char passengersToken[250];
    int k = 0;
    while (fgets(myline, 500, fichier) != NULL) {
        if (i == 0) {
            i++;
        } else {
            sscanf(myline, "%d,%20[^,],%20[^,],%d,%d,%d,%d,%d,%d,%d,%20[^,]",
                   &vols[i].numero, vols[i].comp, vols[i].dest, &vols[i].num_compt,
                   &vols[i].heure_deb_enr, &vols[i].heure_fin_enr, &vols[i].num_salle,
                   &vols[i].heure_deb_emb, &vols[i].heure_fin_emb, &vols[i].heure_deco,
                   vols[i].etat);
            //printf("%s\n",myline);
            substring(myline, passengersToken);
            //printf("%s\n",passengersToken);
            k = 0;
            int m = (compteur(passengersToken) + k);
            int cpt = k;
            //printf("la valeur de m : %d\n",m);
            //printf("la valeur de k :%d\n",k);
            while (k < m) {
                substring2(passengersToken, k, cpt, passengersInfo);
                //printf("%s\n",passengersInfo);
                sscanf(passengersInfo, "%20[^,],%20[^,],%d/%d/%d,%d,%lf",
                       vols[i].passagers[k].nom, vols[i].passagers[k].prenom,
                       &vols[i].passagers[k].date_naissance.jours,
                       &vols[i].passagers[k].date_naissance.mois,
                       &vols[i].passagers[k].date_naissance.annee,
                       &vols[i].passagers[k].siege, &vols[i].passagers[k].prix);

                /*printf("%s\n", vols[i].passagers[k].nom);
                printf("%s\n", vols[i].passagers[k].prenom);
                printf("%d\n", vols[i].passagers[k].date_naissance.jours);
                printf("%d\n", vols[i].passagers[k].date_naissance.mois);
                printf("%d\n", vols[i].passagers[k].date_naissance.annee);
                printf("%d\n", vols[i].passagers[k].siege);
                printf("%.2lf\n", vols[i].passagers[k].prix);
                printf(" la valeur de k: %d\n", k);*/
                k++;
            }
            i++;
        }
    }



    fclose(fichier);
    /*printf("%d\n", vols[1].numero);
    printf("%s\n", vols[1].comp);
    printf("%s\n", vols[1].dest);
    printf("%d\n", vols[1].num_compt);
    printf("%d\n", vols[1].heure_deb_enr);
    printf("%d\n", vols[1].heure_fin_enr);
    printf("%d\n", vols[1].num_salle);
    printf("%d\n", vols[1].heure_deb_emb);
    printf("%d\n", vols[1].heure_fin_emb);
    printf("%s\n", vols[1].etat);*/


    //free(vols);

    return 0;
}

void substring(char myline[], char passengersToken[]) {
    int i, j = 0;
//memset(passengersToken,0,sizeof(passengersToken));
    for (i = 0; i < strlen(myline); i++) {
        if (myline[i] == '\"') {
            i++;
            while (myline[i] != '\"') {
                passengersToken[j] = myline[i];
                j++;
                i++;

            }
        }
    }
    passengersToken[j] = ';';
    passengersToken[j + 1] = '\0';
}

void substring2(char passengersToken[], int k, int cpt, char passengersInfo[]) {
    int i = 0, j = 0, r = 0;
//memset(passengersInfo,0,sizeof(passengersInfo));
    while (i < strlen(passengersToken)) {
        if (passengersToken[i] == ';') {
            cpt++;
            i++;
        }
        if (k == 0) {
            while (passengersToken[i] != ';') {
                passengersInfo[j] = passengersToken[i];
                j++;
                i++;

            }
            break;
        } else if (k == cpt && k != 0) {
            while (passengersToken[i] != ';') {
                passengersInfo[j] = passengersToken[i];
                j++;
                i++;

            }
            break;
        } else {
            i++;
        }
    }
}

int compteur(char passengersToken[]) {
    int nb_pers = 0;
    for (int i = 0; i < strlen(passengersToken); i++) {
        if (passengersToken[i] == ';') {
            nb_pers++;
        }
    }
    nb_pers = nb_pers;
    return nb_pers;

}
